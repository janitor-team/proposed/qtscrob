<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>About</name>
    <message>
        <location filename="../qt/src/about.cpp" line="26"/>
        <source>QTScrobbler v%1</source>
        <translation>QTScrobbler v%1</translation>
    </message>
</context>
<context>
    <name>BrowseDirtreeFrm</name>
    <message>
        <source>Find Directory</source>
        <translation type="obsolete">Wybierz katalog</translation>
    </message>
    <message>
        <source>Browse to the destination folder</source>
        <translation type="obsolete">Wybierz punkt montowania odtwarzacza</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Anuluj</translation>
    </message>
</context>
<context>
    <name>Conf</name>
    <message>
        <location filename="../lib/conf.cpp" line="61"/>
        <source>Conf: Asked to use a blank custom config location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/conf.cpp" line="68"/>
        <source>Conf: Custom config parent folder does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/conf.cpp" line="256"/>
        <source>Conf: Access Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/conf.cpp" line="259"/>
        <source>Conf: Format Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Console</name>
    <message>
        <location filename="../qt/src/console.cpp" line="24"/>
        <source>Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/console.cpp" line="32"/>
        <source>Log Level:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/console.cpp" line="52"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GetTrackInfo</name>
    <message>
        <location filename="../lib/gettrackinfo.cpp" line="73"/>
        <source>API: Failed to download track length: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/gettrackinfo.cpp" line="84"/>
        <source>API: Empty XML</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../qt/src/help.cpp" line="25"/>
        <source>QTScrobbler Help</source>
        <translation>Pomoc</translation>
    </message>
</context>
<context>
    <name>MissingTimeProgress</name>
    <message>
        <location filename="../qt/src/missingtimeprogress.cpp" line="32"/>
        <source>Fetching missing track lengths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/missingtimeprogress.cpp" line="33"/>
        <source>Please wait</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Parse_Ipod</name>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="52"/>
        <source>Parsing failed - unable to open iTunesDB at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="82"/>
        <source>Parsing failed - not an iTunesCDB file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="133"/>
        <source>Parsing failed - not an iTunesDB file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="280"/>
        <source>Unknown blocktype: %1%2%3%4 (0x%5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="297"/>
        <source>Parsing failed - unable to open Play Counts at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="308"/>
        <source>Parsing failed - invalid Play Counts file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="322"/>
        <source>No tracks have been played - possible Apple bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="330"/>
        <source>Different number of songs in Play Counts and iTunesDB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="380"/>
        <source>Found %1 entries, %2 of %3 tracks had playcount information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="399"/>
        <source>Asked to clear without successfully parsing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="404"/>
        <source>Deleted log file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-ipod.cpp" line="407"/>
        <source>Failed to remove log file %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Parse_Log</name>
    <message>
        <location filename="../lib/parse-log.cpp" line="37"/>
        <source>Parsing failed - could not find a logfile at %1 to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="54"/>
        <source>Parsing failed - unable to read file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="63"/>
        <source>Parsing failed - log file is empty or corrupt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="85"/>
        <source>Unknown log version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="92"/>
        <source>Parsing failed - missing #AUDIOSCROBBLER header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="115"/>
        <source>Unknown log TZ: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="123"/>
        <source>Parsing failed - missing #TZ header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="138"/>
        <source>Parsing failed - missing #CLIENT header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="181"/>
        <source>Successfully parsed %1 log entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="190"/>
        <source>Asked to clear without successfully parsing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="195"/>
        <source>Deleted log file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-log.cpp" line="198"/>
        <source>Failed to remove log file %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Parse_MTP</name>
    <message>
        <location filename="../lib/parse-mtp-libmtp.cpp" line="51"/>
        <location filename="../lib/parse-mtp-win32.cpp" line="90"/>
        <source>MTP: No devices have been found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-libmtp.cpp" line="55"/>
        <source>MTP: Error connecting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-libmtp.cpp" line="59"/>
        <source>MTP: Memory Allocation Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-libmtp.cpp" line="66"/>
        <source>MTP: Unknown error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-libmtp.cpp" line="79"/>
        <location filename="../lib/parse-mtp-win32.cpp" line="104"/>
        <source>MTP: Finished parsing tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-libmtp.cpp" line="86"/>
        <location filename="../lib/parse-mtp-win32.cpp" line="110"/>
        <source>MTP: Finished clearing tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-win32.cpp" line="49"/>
        <source>Failed to CoCreate: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-win32.cpp" line="58"/>
        <source>Failed to get number of devices: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/parse-mtp-win32.cpp" line="72"/>
        <source>MTP: Failed to get devices: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <location filename="../qt/src/progress.cpp" line="35"/>
        <source>Submission Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/progress.cpp" line="39"/>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
</context>
<context>
    <name>QTScrob</name>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="98"/>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="100"/>
        <source>Exit</source>
        <translation>Zakończ</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="102"/>
        <source>About</source>
        <translation>O Programie</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="103"/>
        <source>AboutQt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="104"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="105"/>
        <source>Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="109"/>
        <source>Open .scrobbler.log</source>
        <translation>Otwórz .scrobbler.log</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="111"/>
        <source>Delete selected item</source>
        <translation>Usuń zaznaczoną pozycję</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="112"/>
        <source>Submit</source>
        <translation>Wyślij</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="101"/>
        <location filename="../qt/src/qtscrob.cpp" line="147"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="192"/>
        <source>QTScrobbler</source>
        <translation>QTScrobbler</translation>
    </message>
    <message>
        <source>Select jukebox mountpoint</source>
        <translation type="obsolete">Wybierz katalog</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="148"/>
        <source>Global</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="38"/>
        <source>Artist</source>
        <translation>Artysta</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="38"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="38"/>
        <source>Title</source>
        <translation>Tytuł</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="38"/>
        <source>TrackNumber</source>
        <translation>Numer Ścieżki</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="38"/>
        <source>Length</source>
        <translation>Długość</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="38"/>
        <source>Played</source>
        <translation>Odtworzone</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="124"/>
        <location filename="../qt/src/qtscrob.cpp" line="126"/>
        <source>Open MTP Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="211"/>
        <source>Date (UTC)</source>
        <translation>Data (UTC)</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="99"/>
        <location filename="../qt/src/qtscrob.cpp" line="110"/>
        <source>Open iTunesDB</source>
        <translation>Otwórz bazę iTunes</translation>
    </message>
    <message>
        <source>Select iPod mountpoint</source>
        <translation type="obsolete">Wybierz katalog montowania iPoda</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="153"/>
        <source>Ready</source>
        <translation>Gotowy</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="156"/>
        <source>Recalc Date/Time</source>
        <translation>Przelicz datę</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="212"/>
        <source>Adjust from (UTC):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="217"/>
        <source>Date (Local)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="218"/>
        <source>Adjust from (Local):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="350"/>
        <source>Select media player drive letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="352"/>
        <source>Select media player mountpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="375"/>
        <source>Error parsing: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="381"/>
        <source>Loaded %1 entries</source>
        <translation>Wczytano %1 elementów</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="515"/>
        <source>Are you sure you want to delete: %1 - %2?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="639"/>
        <source>Submitting data to server...</source>
        <translation>Wysyłanie informacji na serwer...</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="662"/>
        <source>Data submitted Ok</source>
        <translation>Dane wysłane pomyślnie</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="668"/>
        <source>There was a problem submitting data to the server
(reason: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="670"/>
        <source>Submission Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There was a problem submitting data to the server
(reason: %1)

Please check your internet connection, or try again later</source>
        <translation type="obsolete">Wystąpił błąd podczas wysyłania danych na serwer
(powód: %1)

Proszę sprawdzić połączenie z internetem lub spróbować później</translation>
    </message>
    <message>
        <source>Submission Failed - server error</source>
        <translation type="obsolete">Wysyłanie nieudane - błąd serwera</translation>
    </message>
    <message>
        <source>Are you sure you want to delete: </source>
        <translation type="obsolete">Czy napewno chcesz usunąć:</translation>
    </message>
    <message>
        <location filename="../qt/src/qtscrob.cpp" line="531"/>
        <source>Are you sure you want to delete the selected items</source>
        <translation>Czy napewno chcesz usunąć zaznaczone pozycje</translation>
    </message>
</context>
<context>
    <name>Scrobble</name>
    <message>
        <location filename="../lib/libscrobble.cpp" line="303"/>
        <source>Fetching missing track lengths from last.fm...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/libscrobble.cpp" line="399"/>
        <source>Nothing to submit!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/libscrobble.cpp" line="410"/>
        <source>One or more tracks are too old.  Correct this and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/libscrobble.cpp" line="457"/>
        <source>No sites were configured and/or enabled. Unable to submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/libscrobble.cpp" line="511"/>
        <source>Asked to create a duplicate parser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/libscrobble.cpp" line="532"/>
        <source>Error - asked to use an unknown Parser!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/libscrobble.cpp" line="588"/>
        <source>Parser: asked to clear without a live object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qt/src/settings.cpp" line="83"/>
        <source>Offset: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/settings.cpp" line="86"/>
        <source>Daylight saving: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/settings.cpp" line="88"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/settings.cpp" line="90"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Site_UI</name>
    <message>
        <location filename="../qt/src/settings.cpp" line="33"/>
        <source>Enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/settings.cpp" line="34"/>
        <source>Site Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/settings.cpp" line="35"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/settings.cpp" line="36"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/settings.cpp" line="37"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Submit</name>
    <message>
        <location filename="../lib/submit.cpp" line="67"/>
        <source>%1: init function hasn&apos;t been run!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="131"/>
        <source>%1: Error, attempted to send an empty submission!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="164"/>
        <source>SUBMIT %1: Request failed, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="201"/>
        <source>%1: Server returned an error after sending data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="242"/>
        <source>%1: Empty result from server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="281"/>
        <source>%1: HANDSHAKE: Request failed: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="293"/>
        <source>%1: Handshake Failed - client software banned.  Check http://qtscrob.sourceforge.net for updates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="301"/>
        <source>%1: Handshake Failed - authentication problem.  Check your username and/or password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="309"/>
        <source>%1: Handshake Failed - clock is incorrect. Check your computer clock and timezone settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="316"/>
        <source>%1: Handshake Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="339"/>
        <source>%1: Bad handshake response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/submit.cpp" line="346"/>
        <source>%1: Unknown handshake response</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>aboutWin</name>
    <message>
        <location filename="../qt/src/ui/aboutWin.ui" line="29"/>
        <source>About QTScrobbler</source>
        <translation>O QTScrobblerze</translation>
    </message>
    <message utf8="true">
        <location filename="../qt/src/ui/aboutWin.ui" line="71"/>
        <source>a Last.fm submitter

Copyright (C) 2006-2009 Tomasz Moń
(C) 2006-2013 Robert Keevil

Thanks to:
Alex Pounds
Mathias Gumz

Please join QTScrobbler group on last.fm</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>a Last.fm submitter

Copyright (C) 2006-2009 Tomasz Moń

Thanks to:
Robert Keevil
Alex Pounds
Mathias Gumz

Please join QTScrobbler group on last.fm</source>
        <translation type="obsolete">Narzędzie do wysyłania logów
z przenośnych odtwarzaczy na Last.fm
Copyright (C) 2006-2008 Tomasz Moń

Podziękowania:
Robert Keevil
Alex Pounds
Mathias Gumz

Proszę dołączyć do grupy QTScrobbler
na last.fm {2006-2009 ?}</translation>
    </message>
    <message>
        <location filename="../qt/src/ui/aboutWin.ui" line="111"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message utf8="true">
        <source>a Last.fm submitter

Copyright (C) 2006-2008 Tomasz Moń

Thanks to:
Robert Keevil
Alex Pounds
Mathias Gumz

Please join QTScrobbler group on last.fm</source>
        <translation type="obsolete">Narzędzie do wysyłania logów
z przenośnych odtwarzaczy na Last.fm
Copyright (C) 2006-2008 Tomasz Moń

Podziękowania:
Robert Keevil
Alex Pounds
Mathias Gumz

Proszę dołączyć do grupy QTScrobbler
na last.fm</translation>
    </message>
</context>
<context>
    <name>app</name>
    <message>
        <location filename="../cli/app.cpp" line="132"/>
        <source>Unknown option argument:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="150"/>
        <source>a Last.fm uploader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="152"/>
        <source>Mandatory arguments to long options are mandatory for short options too.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="154"/>
        <source>Required:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="155"/>
        <source>Location to the config file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="156"/>
        <source>The config file holds the site configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="157"/>
        <source>(usernames, passwords), proxy info, timezone offsets etc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="159"/>
        <source>Methods:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="160"/>
        <source>Submit ipod database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="161"/>
        <source>Submit .scrobbler.log file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="163"/>
        <source>Submit MTP device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="167"/>
        <source>Mount point of the player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="168"/>
        <source>UNIX timestamp to recalculate from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="169"/>
        <source>Re-calculate the play time to now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="170"/>
        <source>Re-calculate the time when tracks were played</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="171"/>
        <source>Verbosity level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="173"/>
        <source>Auto-detected timezone info:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="174"/>
        <source>Timezone: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="175"/>
        <source>Offset: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="176"/>
        <source>Daylight saving: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="190"/>
        <source>You can only use one of -n and -r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="227"/>
        <source>Error - no (single) method specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="242"/>
        <source>Error parsing data: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="261"/>
        <source>Submission failed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli/app.cpp" line="263"/>
        <source>Submission complete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>settingsWin</name>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="32"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Username:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Użytkownik:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Password:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Hasło:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Zastosuj</translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="558"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="271"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Timezone:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Czas:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>UTC -12</source>
        <translation type="obsolete">UTC -12</translation>
    </message>
    <message>
        <source>UTC -11</source>
        <translation type="obsolete">UTC -11</translation>
    </message>
    <message>
        <source>UTC -10</source>
        <translation type="obsolete">UTC -10</translation>
    </message>
    <message>
        <source>UTC -9</source>
        <translation type="obsolete">UTC -9</translation>
    </message>
    <message>
        <source>UTC -8</source>
        <translation type="obsolete">UTC -8</translation>
    </message>
    <message>
        <source>UTC -7</source>
        <translation type="obsolete">UTC -7</translation>
    </message>
    <message>
        <source>UTC -6</source>
        <translation type="obsolete">UTC -6</translation>
    </message>
    <message>
        <source>UTC -5</source>
        <translation type="obsolete">UTC -5</translation>
    </message>
    <message>
        <source>UTC -4</source>
        <translation type="obsolete">UTC -4</translation>
    </message>
    <message>
        <source>UTC -3</source>
        <translation type="obsolete">UTC -3</translation>
    </message>
    <message>
        <source>UTC -2</source>
        <translation type="obsolete">UTC -2</translation>
    </message>
    <message>
        <source>UTC -1</source>
        <translation type="obsolete">UTC -1</translation>
    </message>
    <message>
        <source>UTC</source>
        <translation type="obsolete">UTC</translation>
    </message>
    <message>
        <source>UTC +1</source>
        <translation type="obsolete">UTC +1</translation>
    </message>
    <message>
        <source>UTC +2</source>
        <translation type="obsolete">UTC +2</translation>
    </message>
    <message>
        <source>UTC +3</source>
        <translation type="obsolete">UTC +3</translation>
    </message>
    <message>
        <source>UTC +4</source>
        <translation type="obsolete">UTC +4</translation>
    </message>
    <message>
        <source>UTC +5</source>
        <translation type="obsolete">UTC +5</translation>
    </message>
    <message>
        <source>UTC +6</source>
        <translation type="obsolete">UTC +6</translation>
    </message>
    <message>
        <source>UTC +7</source>
        <translation type="obsolete">UTC +7</translation>
    </message>
    <message>
        <source>UTC +8</source>
        <translation type="obsolete">UTC +8</translation>
    </message>
    <message>
        <source>UTC +9</source>
        <translation type="obsolete">UTC +9</translation>
    </message>
    <message>
        <source>UTC +10</source>
        <translation type="obsolete">UTC +10</translation>
    </message>
    <message>
        <source>UTC +11</source>
        <translation type="obsolete">UTC +11</translation>
    </message>
    <message>
        <source>UTC +12</source>
        <translation type="obsolete">UTC +12</translation>
    </message>
    <message>
        <source>UTC +13</source>
        <translation type="obsolete">UTC +13</translation>
    </message>
    <message>
        <source>UTC +14</source>
        <translation type="obsolete">UTC +14</translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="45"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="59"/>
        <source>Automatically open log</source>
        <translation>Automatycznie otwórz log</translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="72"/>
        <source>Auto-delete Apple Playcount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="85"/>
        <source>Display time as UTC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="108"/>
        <source>Sites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="128"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="145"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="161"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="177"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="193"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="206"/>
        <source>Timezone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="218"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Detected Timezone:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="250"/>
        <source>Manual Override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="297"/>
        <source>-12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="302"/>
        <source>-11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="307"/>
        <source>-10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="312"/>
        <source>-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="317"/>
        <source>-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="322"/>
        <source>-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="327"/>
        <source>-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="332"/>
        <source>-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="337"/>
        <source>-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="342"/>
        <source>-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="347"/>
        <source>-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="352"/>
        <source>-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="357"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="362"/>
        <source>+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="367"/>
        <source>+2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="372"/>
        <source>+3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="377"/>
        <source>+4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="382"/>
        <source>+5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="387"/>
        <source>+6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="392"/>
        <source>+7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="397"/>
        <source>+8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="402"/>
        <source>+9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="407"/>
        <source>+10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="412"/>
        <source>+11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="417"/>
        <source>+12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="422"/>
        <source>+13</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="427"/>
        <source>+14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="441"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="470"/>
        <source>00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="475"/>
        <source>15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="480"/>
        <source>30</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="485"/>
        <source>45</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qt/src/ui/settingsWin.ui" line="531"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Port:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Port:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Host:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:8pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Host:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
</TS>
